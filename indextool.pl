#!/usr/bin/perl
# Pleiar.no review index tool
# Copyright (C) Fagforbundet 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use 5.014;
use warnings;
use JSON::DWIW;    # Needs this JSON module for proper utf8
use Try::Tiny;
use File::Find;
use File::Basename qw(dirname basename);
use Cwd            qw(getcwd);
use IO::All -utf8;
use IPC::Open3;
use Getopt::Long;
use Fatal qw(chdir);
use YAML  qw(Load Dump);
use POSIX qw(strftime);
use Email::Stuffer;
use Email::Sender::Transport::SMTP::Persistent;
use Authen::SASL
  ;    # Not used directly, required to be available by Email::Sender for SSL
use utf8;
use open qw( :encoding(UTF-8) :std );
use feature 'signatures';
no warnings 'experimental::signatures';

my $verbosity = 0;

use constant {
    true  => 1,
    false => undef,
};
my %allowedUserMetaKeys = (
    lastReviewedRev  => 1,
    lastReviewedBy   => 1,
    lastReviewedDate => 1,
    reviewOwner      => 1,
);

# Outputs $msg in verbose mode
sub printv ($msg) {
    if ( $verbosity > 0 ) {
        say $msg;
    }
}

# Runs @command and reads all of the output from it, then it returns this
# output as a scalar
sub slurpCommand (@command) {
    my $pid  = open3( my $in, my $out, my $err, @command );
    my $data = '';
    while (<$out>) {
        $data .= $_;
    }
    waitpid( $pid, 0 );
    close($in);
    close($out);
    close($err) if $err;
    chomp($data);
    return $data;
}

# Constructs a handbook article title from the article metadata
sub constructTitleFromMeta ( $meta, $file ) {
    my @elements = split( "/", $file );
    my $curr     = './';
    my @titles;
    foreach my $el (@elements) {
        next if $el eq '.';
        $curr .= $el;
        my $title = $meta->{$curr};
        if ( $title && $title ne 'Root' ) {
            push( @titles, $meta->{$curr} );
        }
        if ( -d "./data/" . $curr ) {
            $curr .= '/';
        }
    }
    return join( '/', @titles );
}

# Gets git info (revision and date changed) for a single file
sub getGitInfo ($file) {
    my $cwd = getcwd();
    my $dir = dirname($file);
    chdir($dir);
    my $rev =
      slurpCommand( qw(git --no-pager log -1 --pretty=%H), basename($file) );
    my $date = slurpCommand( qw(git --no-pager log -1 --date=unix --pretty=%ad),
        basename($file) );
    chdir($cwd);
    return {
        rev        => $rev,
        changeDate => $date,
    };
}

# Gets the date a file was changed from git, in unixtime
sub getGitRevisionDate ($rev) {
    if ( $rev eq '0' || length($rev) < 39 ) {
        return 0;
    }
    my $cwd = getcwd();
    chdir('./data');
    my $date =
      slurpCommand( qw(git --no-pager log -1 --date=unix --pretty=%ad), $rev );
    chdir($cwd);
    return $date;
}

# Loads the review metadata file, if it exists
sub loadMetaFile {
    my $file = io('./data/reviewMeta.yml');
    if ( !$file->exists ) {
        return {};
    }
    my $content = $file->slurp   or die;
    my $meta    = Load($content) or die;
    return $meta;
}

# Outputs a file in the metadata format. Used to update the metadata file when
# more content has been added.
sub dumpMetaFile ($meta) {
    my %metaForDump;
    foreach my $file ( keys %{$meta} ) {
        $metaForDump{$file} = {};
        foreach my $key ( keys %allowedUserMetaKeys ) {
            if ( defined $meta->{$file}->{$key} ) {
                $metaForDump{$file}->{$key} = $meta->{$file}->{$key};
            }
        }
    }
    io('./data/reviewMeta.yml')->print( Dump( \%metaForDump ) );
    say 'Wrote meta file to ./data/reviewMeta.yml';
}

# Builds the index.json document index used by the review tool. Returns a
# datastructure.
sub buildDocumentIndex {
    my %ignoreFiles = ( './README.md' => 1 );
    my %ymlFiles    = (
        'AndreSider.yml' => 'Andre Sider',
        'Labverdier.yml' => 'Lab verdiar',
        'Ordliste.yml'   => 'Ordliste',
    );
    my %files;
    my %metaFileTitles;

    my $cwd = getcwd();
    chdir('./data/');
    find(
        {
            follow   => false,
            no_chdir => true,
            wanted   => sub {
                my $file = $File::Find::name;
                if ( $file =~ /\.git/ ) {
                    return;
                }
                if ( $ignoreFiles{$file} ) {
                    return;
                }
                if ( index( $file, '/node_modules' ) != -1 ) {
                    return;
                }
                if ( $file =~ /~$/ ) {
                    return;
                }
                printv("Processing $file");
                my $baseFile = basename($file);
                if ( $baseFile eq 'Metadata.yml' ) {
                    my $dir     = dirname($file);
                    my $content = io($file)->slurp or die;
                    my $HBMeta  = Load($content)   or die;
                    if ( $HBMeta->{title} ) {
                        $metaFileTitles{$dir} = $HBMeta->{title};
                    }
                    foreach my $section ( keys %{ $HBMeta->{sections} } ) {
                        if ( defined $HBMeta->{sections}->{$section}->{title} )
                        {
                            $metaFileTitles{ $dir . '/' . $section } =
                              $HBMeta->{sections}->{$section}->{title};
                        }
                    }
                }
                elsif ($file =~ /\.(md|yml)$/
                    && -e $file
                    && !-d $file
                    && !-l $file )
                {
                    if ( $file =~ /\.yml$/ && !$ymlFiles{$baseFile} ) {
                        return;
                    }
                    my $metaInfo = getGitInfo($file);
                    $metaInfo->{lastReviewedRev}     = 0;
                    $metaInfo->{lastReviewedRevDate} = 0;
                    $metaInfo->{lastReviewedBy}      = 'unknown';
                    $metaInfo->{lastReviewedDate}    = 0;
                    $metaInfo->{reviewOwner}         = 'unknown';
                    my $prettyFile = $file;
                    $prettyFile =~ s{^\.\/}{};

                    if ( $ymlFiles{$baseFile} ) {
                        $metaInfo->{title} =
                          $ymlFiles{$baseFile} . ' (' . $prettyFile . ')';
                    }
                    else {
                        $metaInfo->{title} = $prettyFile;
                    }
                    $files{$file} = $metaInfo;
                }
            }
        },
        './'
    );
    chdir($cwd);

    my %people;
    my $meta = loadMetaFile();
    foreach my $file ( keys %{$meta} ) {
        if ( $files{$file} ) {
            my $entry = $files{$file};
            foreach my $key ( keys %allowedUserMetaKeys ) {
                if ( defined $meta->{$file}->{$key} ) {
                    $entry->{$key} = $meta->{$file}->{$key};
                }
            }
            my $revDate =
              getGitRevisionDate( $files{$file}->{lastReviewedRev} );
            if (   $entry->{lastReviewedRev} ne $entry->{rev}
                && $entry->{lastReviewedDate} < $entry->{changeDate} )
            {
                my $diffEntry = $entry->{lastReviewedRev};
                if ( $diffEntry eq '0' ) {
                    $diffEntry = '7ea51eaeccd381218c7cb56b82f0e300caf14cd8';
                }
                chdir('./data/');
                $entry->{diff} = slurpCommand( qw(git --no-pager diff),
                    $diffEntry, 'HEAD', '--', $file );
                chdir($cwd);
            }
            $files{$file}->{lastReviewedRevDate}       = $revDate;
            $people{ $files{$file}->{lastReviewedBy} } = 1;
            $people{ $files{$file}->{reviewOwner} }    = 1;
        }
    }
    delete $people{'unknown'};
    foreach my $file ( keys %metaFileTitles ) {
        if ( $files{$file} ) {
            $files{$file}->{title} =
              constructTitleFromMeta( \%metaFileTitles, $file );
        }
    }
    return {
        files  => \%files,
        people => [ sort keys %people ],
    };
}

# Sends notification e-mails to reviewers with outstanding reviews
sub sendNotificationMails ( $index, $doNotSend ) {
    say "indextool: sendNotificationMails running...";
    my $peopleMailMapFile = io('./data/Fagpanel.yml')->slurp or die;
    my $peopleMailMap     = Load($peopleMailMapFile)         or die;
    my $errorOut          = 0;
    foreach my $person ( @{ $index->{people} } ) {
        my $skip     = 0;
        my $assigned = 0;
        my $mail     = $peopleMailMap->{$person};
        if ( !defined $mail ) {
            warn("Failed to find e-mail for: $person\n");
            $errorOut = 1;
            next;
        }
        my @forReview;
        foreach my $file ( sort keys %{ $index->{files} } ) {
            my $current = $index->{files}->{$file};
            next if $current->{reviewOwner} ne $person;
            if (   $current->{lastReviewedDate} < ( time - 23328000 )
                || $current->{changeDate} > $current->{lastReviewedDate} )
            {
                push( @forReview, "- " . $current->{title} );
                $assigned++;
                if ( $assigned >= 5 ) {
                    last;
                }
            }
        }
        if ( defined $ENV{PR_SKIP_USERS} && length( $ENV{PR_SKIP_USERS} ) > 0 )
        {
            foreach my $skipEntry ( split( /\s+/, $ENV{PR_SKIP_USERS} ) ) {
                if ( $skipEntry eq $mail ) {
                    say
                      "sendSingleMail: skipping $mail because of PR_SKIP_USERS";
                    $skip = 1;
                    last;
                }
            }
        }
        if ( $assigned > 0 && !$doNotSend && !$skip ) {
            say "Sending notification mail to $mail";
            sendSingleMail(
                $person . ' <' . $mail . '>',
                'Pleiar.no gjennomgang for ' . strftime( "%Y-%m", localtime ),
                "Hei "
                  . $person . "!\n\n"
                  . "Følgende artikler på pleiar.no har behov for å bli gått gjennom:\n"
                  . join( "\n", @forReview ) . "\n\n"
                  . "Verktøyet for gjennomgang finner du på https://review.pleiar.no/\n\n"
                  . "Med hilsen,\nGjennomgangsroboten"
            );
        }
    }
    if ($errorOut) {
        exit(1);
    }
}

# Sends a single e-mail to someone
sub sendSingleMail ( $to, $subject, $body ) {
    foreach my $required (
        qw(PR_MAIL_HOST PR_MAIL_PORT PR_MAIL_USER PR_MAIL_PWD PR_MAIL_SENDER))
    {
        if ( !defined $ENV{$required} || !length( $ENV{$required} ) ) {
            die("Required env var not present: $required\n");
        }
    }
    state $transport = Email::Sender::Transport::SMTP->new(
        {
            ssl           => true,
            host          => $ENV{PR_MAIL_HOST},
            port          => $ENV{PR_MAIL_PORT},
            timeout       => 300,
            sasl_username => $ENV{PR_MAIL_USER},
            sasl_password => $ENV{PR_MAIL_PWD},
        }
    );
    my $message = Email::Stuffer->new(
        {
            from      => $ENV{PR_MAIL_SENDER},
            to        => $to,
            subject   => $subject,
            text_body => $body,
        }
    );

    try {
        $message->transport($transport);
        $message->send_or_die($transport);
    }
    catch {
        die("Failed to send e-mail: $_");
    };
}

# Main function
sub main {
    my $writeMetaFile         = 0;
    my $denyMailSending       = 0;
    my $sendNotificationMails = 0;
    my $outputSummaryToSTDOUT = 0;
    GetOptions(
        '--write-meta'              => \$writeMetaFile,
        '--send-notification-mails' => \$sendNotificationMails,
        'verbose'                   => \$verbosity,
        '--validate-mails'          => sub {
            $sendNotificationMails = 1;
            $denyMailSending       = 1;
        },
        '--stdout' => \$outputSummaryToSTDOUT,
        '--help'   => sub {
            say "pleiar.no review index tool\n";
            say "Usage: indextool.pl [parameters]\n";
            say
"If no parameters are provided, then the index.json file will be built.\n";
            say "Parameters:";
            say " --write-meta               Updates ./data/reviewMeta.yml";
            say
" --send-notification-mails  Notifies reviewers about outstanding files";
            say
" --validate-mails           Validate that all required e-mail addresses are";
            say "                            present in ./data/Fagpanel.yml";
            say
" --stdout                   Output a listing of files and change dates to";
            say
"                            STDOUT instead of writing index.json";
            say " --help                     Output this help information";
            exit(0);
        },
    ) or die("Invalid command-line arguments\n");
    my $index = buildDocumentIndex();
    if ($writeMetaFile) {
        dumpMetaFile( $index->{files} );
    }
    elsif ($sendNotificationMails) {
        sendNotificationMails( $index, $denyMailSending );
        if ($denyMailSending) {
            say "All e-mails validated";
        }
    }
    elsif ($outputSummaryToSTDOUT) {
        my @output;
        foreach my $fileName ( sort keys %{ $index->{files} } ) {
            my $meta = $index->{files}->{$fileName};
            my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )
              = localtime( $meta->{changeDate} );
            $year += 1900;
            $mon++;
            my $date = sprintf( "%d-%.2d-%.2d", $year, $mon, $mday );

            push( @output, sprintf( "%-10s %s", $date, $meta->{title}, ) );
        }

        say join( "\n", sort(@output) );
    }
    else {
        say 'Wrote index to ./index.json';
        io('./index.json')->utf8->write( JSON::DWIW->to_json($index) );
    }
}

main();
