/* global module __dirname process */
/* eslint-disable flowtype/require-return-type */
/* eslint-disable flowtype/require-variable-type */
/* eslint-disable flowtype/require-valid-file-annotation */
/* eslint-disable require-jsdoc */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');
const utf8tob64 = require('jsrsasign').utf8tob64;
const childProcess = require('child_process');

const GIT_REVISION = childProcess.execSync('git rev-parse --short=4 HEAD').toString().replace(/\n/g,'');
let pwd = "review";
let mailto = "example@example.org";
if(process.env.PR_PWD)
{
    pwd = process.env.PR_PWD;
}
if(process.env.PR_MAIL)
{
    mailto = process.env.PR_MAIL;
}

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader",
                        // Disables url() handling. Just assume they are correct.
                        options: { url: false }
                    },
                    {
                        loader: "sass-loader",
                    }
                ],
            },
            {
                test: /\.yml$/,
                use: [
                    {
                        loader: "json-loader"
                    },
                    {
                        loader: "yaml-loader"
                    },
                ],
            },
            {
                test: /\.md$/,
                use: [
                    'raw-loader',
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    entry: {
        main: './src/index.js',
    },
    output: {
        filename: '[name].bundle.js?b=[chunkhash]',
        path: path.resolve(__dirname, 'public'),
        publicPath:'/',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            },
        }),
        new CopyWebpackPlugin([
            { from:'static/' },
            { from:'data/static/' },
            { from:'node_modules/@babel/polyfill/dist/polyfill.min.js', to:'babel-polyfill.min.js' },
        ]),
        new webpack.DefinePlugin({
            'VALID_PWDS':JSON.stringify(utf8tob64(JSON.stringify([ pwd ]))),
            'MAILTO':JSON.stringify(mailto),
            'GIT_REVISION':JSON.stringify(GIT_REVISION),
        }),
        new MiniCssExtractPlugin({
        })
    ],
    optimization:
    {
        runtimeChunk: "single",
    },
    performance:
    {
        hints: false
    },

    devServer: {
        contentBase: "./public.devserver",
        historyApiFallback: true,
        inline: true
    }
};
