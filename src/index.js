/*
 * Part of pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Container, UncontrolledTooltip, Row, Col, Button, InputGroup, InputGroupAddon, Input, Form, Table, Label, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import dayjs from 'dayjs';
import 'dayjs/locale/nn';
import relativeTime from 'dayjs/plugin/relativeTime';
import { b64toutf8} from 'jsrsasign';
// $FlowProblem - flow for some reason doesn't find the module
import { pleiarLogin } from '../data/auth';

import UpIcon from 'react-feather/dist/icons/chevron-up';
import DownIcon from 'react-feather/dist/icons/chevron-down';

// $FlowProblem
import './style.scss';
// Let flow know about our VALID_PWDS, MAILTO and GIT_REVISION globals (from webpack)
declare var VALID_PWDS:string;
declare var MAILTO:string;
declare var GIT_REVISION:string;
import data from '../index.json';
dayjs.extend(relativeTime);
dayjs.locale('nn');

/**
 * Converts a string (generally a file path) into something usable as an `id=` attribute
 */
function idIfy (entry: string): string
{
    return entry.replace(/[/.]/g,'_');
}

/**
 * Converts the string "unknown" into "(ingen)"
 */
function nameIfy (name: string): string
{
    if(name === "unknown")
    {
        return "(ingen)";
    }
    return name;
}

type SortDirectionType = "normal" | "reverse";
type SortColumnType = "changeDate" | "lastReviewedBy" | "reviewOwner" | "title";
type PleiarReviewerState = {|
    filterOwner: ?string,
    filterReviewer: ?string,
    onlyChanged: boolean,
    sortColumn: SortColumnType,
    sortDirection: SortDirectionType,
    viewDiff: string,
|};

type filterThingProps = {|
    onChange: (SyntheticInputEvent<>) => void,
    entries: Array<string>,
    selected: ?string,
|};

/**
 * A simple class to render a select element
 */
class FilterThing extends React.Component<filterThingProps>
{
    render () // eslint-disable-line require-jsdoc
    {
        const { onChange, entries, selected } = this.props;
        const options = [
            <option key="base" value="">Vis alle</option>
        ];
        for(const entry of entries)
        {
            options.push(<option key={entry} value={entry}>{entry}</option>);
        }
        return <select value={selected} onChange={onChange}>{options}</select>;
    }
}

type SortableHeaderProps = {|
    selected: string,
    name: string,
    direction: SortDirectionType,
    column: SortColumnType,
    onChange: () => void,
|};

type DiffViewerProps = {|
    open: boolean,
    diffSource: string,
    toggle: () => void,
|};
/**
 * A modal dialog that displays a diff
 */
class DiffViewer extends React.Component<DiffViewerProps>
{
    render () // eslint-disable-line require-jsdoc
    {
        const { diffSource, open, toggle } = this.props;
        const source = data.files[diffSource];
        const diff = (source && source.diff) ? source.diff : "";
        const diffLines = diff.split("\n");
        const prettyFile = diffSource.replace('./','');
        const renderDiff: Array<React.Node> = [];
        for(let no: number = 0; no < diffLines.length; no++)
        {
            const line = diffLines[no];
            if (/^\+/.test(line))
            {
                renderDiff.push(<div key={no} className="text-success">{line}</div>);
            }
            else if (/^-/.test(line))
            {
                renderDiff.push(<div key={no} className="text-danger">{line}</div>);
            }
            else
            {
                renderDiff.push(<div key={no}>{line}</div>);
            }
        }
        return <Modal isOpen={open} toggle={toggle} backdrop={true}>
            <ModalHeader toggle={toggle}>Endringar i {prettyFile}</ModalHeader>
            <ModalBody>
                <pre>{renderDiff}</pre>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={toggle}>Lukk</Button>
            </ModalFooter>
        </Modal>;
    }
}

/**
 * Outputs a single `<th>` header, that can be toggled as sortable
 */
class SortableHeader extends React.Component<SortableHeaderProps>
{
    render () // eslint-disable-line require-jsdoc
    {
        const { column, selected, name, direction, onChange } = this.props;
        let arrow: React.Node;
        if (selected === column)
        {
            if(direction === "normal")
            {
                arrow = <UpIcon />;
            }
            else
            {
                arrow = <DownIcon />;
            }
        }
        return <th onClick={onChange}>{name}{arrow}</th>;
    }
}

/**
 * The actual UI
 */
class PleiarReviewer extends React.Component<{||},PleiarReviewerState>
{
    state = {
        filterOwner: "",
        filterReviewer:"",
        sortColumn: "changeDate",
        sortDirection: "normal",
        onlyChanged: false,
        viewDiff: ""
    };

    /**
     * Returns a callback that can be used for SortableHeader
     */
    SortableCallback (field: SortColumnType): () => void
    {
        return () => {
            let direction: SortDirectionType = "normal";
            if(field === this.state.sortColumn)
            {
                if(this.state.sortDirection === direction)
                {
                    direction = "reverse";
                }
                this.setState({
                    sortDirection: direction
                });
            }
            else
            {
                if(field !== "changeDate")
                {
                    direction = "reverse";
                }
                this.setState({
                    sortDirection: direction,
                    sortColumn: field
                });
            }
        };
    }

    render () // eslint-disable-line require-jsdoc
    {
        const fileData = data.files;
        const table = [];
        let files: Array<string> = Object.keys(fileData).sort((a,b) => {
            const fileA = fileData[a];
            const fileB = fileData[b];
            if(fileA[ this.state.sortColumn ] < fileB[ this.state.sortColumn ])
            {
                return 1;
            }
            if(fileA[ this.state.sortColumn ] > fileB[ this.state.sortColumn ])
            {
                return -1;
            }
            if(a < b)
            {
                return 1;
            }
            if(a > b)
            {
                return -1;
            }
            return 0;
        });
        if(this.state.sortDirection === "reverse")
        {
            files = files.reverse();
        }
        if(this.state.filterOwner !== "")
        {
            files = files.reduce((accumulator: Array<string>, currentValue: string) => {
                const file = fileData[currentValue];
                if(file && file.reviewOwner === this.state.filterOwner)
                {
                    accumulator.push(currentValue);
                }
                return accumulator;
            },[]);
        }
        if(this.state.filterReviewer !== "")
        {
            files = files.reduce((accumulator: Array<string>, currentValue: string) => {
                const file = fileData[currentValue];
                if(file && file.lastReviewedBy === this.state.filterReviewer)
                {
                    accumulator.push(currentValue);
                }
                return accumulator;
            },[]);
        }
        if(this.state.onlyChanged)
        {
            files = files.reduce((accumulator: Array<string>, currentValue: string) => {
                const file = fileData[currentValue];
                if(file && file.changeDate > file.lastReviewedDate)
                {
                    accumulator.push(currentValue);
                }
                return accumulator;
            },[]);
        }
        const nineMonthsAgo = ( Math.floor(Date.now() / 1000) - 23328000);
        for(const fileName of files)
        {
            const prettyFile = fileName.replace('./','');
            const ID = idIfy(fileName);
            const file = fileData[fileName];
            // $FlowProblem - flow doesn't know that we've extended dayjs
            const prettyChangeDate = dayjs.unix(file.changeDate).fromNow();
            let title: React.Node;
            if(prettyFile.indexOf('handbok') === 0)
            {
                const URL = prettyFile.replace(/\d+-/g,'').replace(/\.md$/g,'');
                title = <a href={'https://www.pleiar.no/'+URL} id={ID+'-title'}>{file.title}</a>;
            }
            else if(prettyFile.indexOf('hurtigoppslag') === 0)
            {
                const URL = prettyFile.replace(/\d+-/g,'').replace(/\.md$/g,'').replace('hurtigoppslag','oppslag');
                title = <a href={'https://www.pleiar.no/'+URL} id={ID+'-title'}>{file.title}</a>;
            }
            else
            {
                title = <span id={ID+'-title'}>{file.title}</span>;
            }
            let mailToReport: React.Node;
            let diff: React.Node;
            if((file.changeDate > file.lastReviewedDate) || (file.lastReviewedDate < nineMonthsAgo))
            {
                const mailto = "mailto:"+MAILTO+"?subject="+encodeURIComponent("Gjennomgang av "+prettyFile+" revisjon "+file.rev)+"&body="+encodeURIComponent("(fjern det som ikke passer:)\nFila er godkjent som den er\nFila trenger følgende endringer før den kan bli godkjent:");
                mailToReport = <a href={mailto}>Send gjennomgangsrapport</a>;
            }
            if(file.diff && file.diff !== "" && file.changeDate > file.lastReviewedDate)
            {
                diff = <div className="likeLink" onClick={ () => this.setState({ viewDiff: fileName }) }>Sjå endringar</div>;
            }
            let lastReviewedDate: string;
            if(file.lastReviewedDate == 0)
            {
                lastReviewedDate = "(aldri)";
            }
            else
            {
                // $FlowProblem - flow doesn't know that we've extended dayjs
                lastReviewedDate = dayjs.unix(file.lastReviewedDate).fromNow();
            }

            table.push(<tr key={fileName}>
                <td className="break-word">
                    {title}
                    <UncontrolledTooltip placement="bottom" target={ID+'-title'}>
                        {prettyFile}
                    </UncontrolledTooltip>
                </td>
                <td>{prettyChangeDate}</td>
                <td>
                    <div>
                        <span id={ID+"-lastReviewedBy"}>{nameIfy(file.lastReviewedBy)}</span>
                        <UncontrolledTooltip placement="bottom" target={ID+'-lastReviewedBy'}>
                            {lastReviewedDate}
                        </UncontrolledTooltip>
                    </div>
                </td>
                <td>{nameIfy(file.reviewOwner)}</td>
                <td className="actionColumn">
                    {mailToReport}
                    {diff}
                </td>
            </tr>);
        }
        return <div>
            <Row>
                <Col>
                    <Button onClick={pleiarLogin} color="secondary">Logg meg inn i pleiar.no</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <b>Hovudansvarleg: </b> <FilterThing entries={data.people} selected={this.state.filterOwner} onChange={(ev) => {
                        this.setState({filterOwner: ev.target.value });
                    }} />
                </Col>
                <Col>
                    <b>Sist gjennomgått av: </b> <FilterThing entries={data.people} selected={this.state.filterReviewer} onChange={(ev) => {
                        this.setState({filterReviewer: ev.target.value });
                    }} />
                </Col>
            </Row>
            <Row>
                <Col style={{marginLeft: "1em" }}>
                    <Label check>
                        <Input type="checkbox" checked={this.state.onlyChanged} onChange={() => { this.setState({ onlyChanged: !this.state.onlyChanged});}} /> Berre vis filer endra sidan sist gjennomgang
                    </Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <DiffViewer toggle={() => this.setState({viewDiff: ""})} open={this.state.viewDiff !== ""} diffSource={this.state.viewDiff} />
                    <Table bordered hover>
                        <thead>
                            <tr>
                                <SortableHeader column="title" selected={this.state.sortColumn} direction={this.state.sortDirection} onChange={this.SortableCallback("title")} name="Tittel" />
                                <SortableHeader column="changeDate" selected={this.state.sortColumn} direction={this.state.sortDirection} onChange={this.SortableCallback("changeDate")} name="Sist endra" />
                                <SortableHeader column="lastReviewedBy" selected={this.state.sortColumn} direction={this.state.sortDirection} onChange={this.SortableCallback("lastReviewedBy")} name="Sist gjennomgått av" />
                                <SortableHeader column="reviewOwner" selected={this.state.sortColumn} direction={this.state.sortDirection} onChange={this.SortableCallback("reviewOwner")} name="Hovudansvarleg" />
                                <th>Handling</th>
                            </tr>
                        </thead>
                        <tbody>
                            {table}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row>
                <Col>
                    <small><a href="https://gitlab.com/fagforbundet/pleiar.no-reviewer">pleiar.no reviewer</a> revision {GIT_REVISION}</small>
                </Col>
            </Row>
        </div>;
    }
}

/**
 * The root handler. Renders the menu and passes control off to the rendering
 * component.
 */
class PleiarReviewerRoot extends React.Component<{||}, {| password: string, invalidPwd: boolean, authenticated: boolean |}>
{
    constructor () // eslint-disable-line
    {
        super();
        this.state = {
            password: '',
            invalidPwd: false,
            authenticated: false,
        };
        (this: $PermitBind).onSetPassword = this.onSetPassword.bind(this);
        (this: $PermitBind).onSubmit = this.onSubmit.bind(this);
    }

    /**
     * Sets the state value storing the password
     */
    onSetPassword (ev)
    {
        this.setState({
            password: ev.currentTarget.value
        });
    }

    /**
     * "Validates" the password and submits auth to the pleiar.no instance if
     * needed
     */
    onSubmit (ev)
    {
        ev.preventDefault();
        const password = this.state.password;
        const validPWDS = JSON.parse(b64toutf8(VALID_PWDS));
        let valid: boolean = false;
        for(const pw of validPWDS)
        {
            if(password == pw)
            {
                valid = true;
                this.setState({
                    authenticated: true
                });
            }
        }
        if(valid === false)
        {
            this.setState({
                invalidPwd: true
            });
            return;
        }
    }

    /**
     * React rendering method
     */
    render () // eslint-disable-line require-jsdoc
    {
        let content: React.Node;
        if(! this.state.authenticated)
        {
            content = <Form inline onSubmit={this.onSubmit}>
                        <InputGroup>
                            <Input className={this.state.invalidPwd ? "is-invalid" : ""} type="password" name="password" id="examplePassword" placeholder="Passord" value={this.state.password} onChange={this.onSetPassword} />
                            <InputGroupAddon addonType="append">
                                <Button>Logg inn</Button>
                            </InputGroupAddon>
                        </InputGroup>
                    </Form>;
        }
        else
        {
            content = <PleiarReviewer />;
        }
        return <Container>
            <Row>
                <Col>
                    <h3>Verkty for gjennomgang av Pleiar.no-innhald</h3>
                </Col>
            </Row>
            <Row>
                <Col>
                    {content}
                </Col>
            </Row>
        </Container>;
    }
}

/**
 * The main initialization function.
 */
function main ()
{
    const root = document.getElementById("root");
    if(root)
    {
        ReactDOM.render(
            <PleiarReviewerRoot />,
            root
        );
    }
    else
    {
        throw('No root element to render into, aborting');
    }
}

main();
