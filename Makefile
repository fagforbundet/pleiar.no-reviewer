# Part of Pleiar.no - a collection of tools for nurses
#
# Copyright (C) Eskild Hustvedt 2017-2018
# Copyright (C) Fagforbundet 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

default: build

SHELL:=/bin/bash
YARN_RUN=yarn run -s
DEVSERVER_PORT=8383

# Builds a complete, deployment-ready version of pleiar.no
build: clean packDist data
ciBuild: build data
# Builds a production version of pleiar.no with webpack
packDist: data
	PLEIAR_ENV=production NODE_ENV=production $(YARN_RUN) webpack --mode production $(PLEIAR_WEBPACK_OPTIONS)
# Cleans up the tree
clean:
	rm -rf build
	rm -f index.json
# Starts a development server
devserverProd: DEVSERVER_PORT=8080
devserverProd: devserver
devserver: BUILD_DATA_ARGS=--verbose
devserver: clean data runDevserver
runDevserver:
	PLEIAR_ENV=development NODE_ENV=development $(YARN_RUN) webpack-dev-server -d --disable-host-check --port $(DEVSERVER_PORT) $(PLEIAR_WEBPACK_DEV_SERVER_ARGS)
.PHONY: data
data:
	perl ./indextool.pl $(BUILD_DATA_ARGS)
updateReviewMeta:
	perl ./indextool.pl --write-meta
flow:
	@if [ -d flow-typed ]; then echo "$(YARN_RUN) flow check --include-warnings"; $(YARN_RUN) flow check --include-warnings;fi
eslint:
	@# Using shell echo to expand the glob before running the rule, so that
	@# it's obvious what eslint is processing.
	$(YARN_RUN) eslint --ignore-pattern node_modules src/ data/
test: eslint flow
	perl -c ./indextool.pl
	perl ./indextool.pl --validate-mails
